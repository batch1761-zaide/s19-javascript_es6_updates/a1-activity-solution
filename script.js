
// alert("Hello")

//1st Part
const num = 9;
const getCube = num ** 3;
console.log(`The cube of 9 is ${getCube}`);


//2nd Part
const address = ["221b", "Baker Street", "Beverly Hills", "California", 90011]

const [houseNum, streetName, cityName, state, zipCode] = address;
console.log(`I live at ${houseNum} ${streetName} ${cityName}, ${state} ${zipCode}`)


//3rd Part

const animal = {
	name: "Perry",
	type: "medium sized peregrine",
	weight: 500,
	length: "1 ft 2 in"
}

const {name, type, weight, length} = animal;

console.log(`${name} is a ${type}. He weighed at ${weight} kgs with a measurement of ${length}.`)

const arrayNumbers = [1,2,3,4,5]

//5th part
arrayNumbers.forEach((numbers) => console.log(`${numbers}`))

class Dog  {
	constructor(name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed;
	}
}

const newDog = new Dog("Goldan", 3 , "Golden Retriever")

console.log(newDog)